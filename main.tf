provider "aws" {
    region = "us-east-1"
}

variable "sub_cidr_block" {
    description = "subnet cidr block"
    default = "10.0.10.0/24"
}

resource "aws_vpc" "dev-vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name: "dev-vpc-tf"
        vpc_env: "dev "
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.dev-vpc.id 
    cidr_block = var.sub_cidr_block
    tags = {
        Name: "dev-subnet-tf"
    }
}
 
output "dev-vpc-id" {
    value = aws_vpc.dev-vpc.id
}

output "dev-sub-id" {
    value = aws_subnet.dev-subnet-1.id
}